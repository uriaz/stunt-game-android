package com.minibit.stuntgame;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;

public class IntroFragment extends Fragment implements OnTouchListener
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.intro_layout, container, false);
		Utils.addTouchListeners(view, this);
		return view;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		Utils.handleTouch(v, event);
		return false;
	}

}
