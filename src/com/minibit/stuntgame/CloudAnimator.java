package com.minibit.stuntgame;

import java.io.IOException;
import java.io.InputStream;

import com.nineoldandroids.animation.ObjectAnimator;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class CloudAnimator
{
	private ImageView[] clouds;
	private int screenWidth, screenHeight;
	private Context context;

	public CloudAnimator(int numClouds, Point screenDims, RelativeLayout rootView)
	{
		clouds = new ImageView[numClouds];
		this.context = rootView.getContext();

		Bitmap[] cloudImages = new Bitmap[numClouds];
		AssetManager assets = context.getAssets();

		String[] fileNames = null;
		try
		{
			fileNames = assets.list("UI Images/Clouds");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		for (int i = 0; i < cloudImages.length; i++)
		{
			InputStream input;
			try
			{
				input = assets.open("UI Images/Clouds/" + fileNames[i]);
				cloudImages[i] = BitmapFactory.decodeStream(input);
				input.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		for (int i = 0; i < Math.min(cloudImages.length, numClouds); i++)
		{
			clouds[i] = new ImageView(context);
			clouds[i].setImageBitmap(cloudImages[i]);
			rootView.addView(clouds[i], 0);
		}

		this.screenWidth = screenDims.x;
		this.screenHeight = screenDims.y;
	}

	public void animateClouds()
	{
		for (int i = 0; i < clouds.length; i++)
		{
			float startX = (float) (-Math.random() * screenWidth);
			float dx = screenWidth - startX;
			float widthRatio = dx / screenWidth;
			long duration = (long) ((Math.random() * 30000 + 30000) * widthRatio);

			ObjectAnimator animator = ObjectAnimator.ofFloat(clouds[i], "x", startX, screenWidth);
			animator.setDuration(duration);
			animator.start();
			animator.setRepeatCount(ObjectAnimator.INFINITE);
			animator.setRepeatMode(ObjectAnimator.RESTART);

			ObjectAnimator yLocSetter = ObjectAnimator.ofFloat(clouds[i], "y", 0, (float) (Math.random() * screenHeight / 4));
			yLocSetter.setDuration(0);
			yLocSetter.start();
		}
	}

}
