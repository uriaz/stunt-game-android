package com.minibit.stuntgame;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public class LevelChooserFragment extends Fragment implements OnTouchListener
{

	private LinearLayout rootView;
	public final static String GAME_MAP_KEY = "gmpky";
	private final static int NUM_BUTTONS = 9;

	private ArrayList<View> levelChooserButtons = new ArrayList<View>();
	private ImageButton slideLeftButton, slideRightButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.level_chooser, container, false);
		rootView = (LinearLayout) view.findViewById(R.id.level_chooser_button_root);
		createLevelButtons(rootView);

		slideLeftButton = (ImageButton) view.findViewById(R.id.slideLeftButton);
		slideRightButton = (ImageButton) view.findViewById(R.id.slideRightButton);

		Utils.addTouchListeners(view.findViewById(R.id.upperRoot), this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
	}

	private final static String LEVEL_BUTTON_IDENTIFIER = "lvlbtnidntfr";

	public void createLevelButtons(LinearLayout rootLayout)
	{
		rootLayout.setOrientation(LinearLayout.VERTICAL);
		rootLayout.setWeightSum(1.0f);

		int numRows = (int) Math.sqrt(NUM_BUTTONS);
		int colSize = NUM_BUTTONS / numRows;

		LinearLayout[] rowLayouts = new LinearLayout[numRows];
		LayoutParams butonLayoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f / colSize);
		LayoutParams rowLinearLayoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f / numRows);

		for (int i = 0; i < numRows; i++)
		{
			rowLayouts[i] = new LinearLayout(getActivity());
			rowLayouts[i].setOrientation(LinearLayout.HORIZONTAL);
			rowLayouts[i].setWeightSum(1.0f);
			rowLayouts[i].setLayoutParams(rowLinearLayoutParams);
			rootLayout.addView(rowLayouts[i]);
		}

		LayoutInflater inflater = ((LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE));
		int rowCounter = 0;

		final float scale = getResources().getDisplayMetrics().density;
		// convert to dp
		int padding = (int) (scale * 5);

		Typeface buttonFont = Typeface.createFromAsset(getActivity().getAssets(), IntroActivity.CARTOON_FONT);
		for (int i = 0; i < NUM_BUTTONS; i++)
		{
			RelativeLayout root = (RelativeLayout) inflater.inflate(R.layout.level_chooser_button, null);
			root.setLayoutParams(butonLayoutParams);

			Button lvlchooser = (Button) root.findViewById(R.id.chooser_button);
			LevelDescriptor descriptor = new LevelDescriptor();
			descriptor.setLevelNum(i + 1);
			lvlchooser.setTag(descriptor);
			lvlchooser.setTypeface(buttonFont);
			lvlchooser.setText(i + 1 + "");

			levelChooserButtons.add(root);

			root.setPadding(padding, padding, padding, padding);
			if (i % colSize == 0 && i != 0)
			{
				rowCounter++;
			}

			rowLayouts[rowCounter].addView(root);
		}
	}

	@Override
	public boolean onTouch(View touchedView, MotionEvent event)
	{
		Utils.handleTouch(touchedView, event);

		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			switch (touchedView.getId())
			{
			case R.id.chooser_button:

				LevelDescriptor descriptor = (LevelDescriptor) touchedView.getTag();
				String mapId = "level" + (descriptor.getLevelNum() + NUM_BUTTONS * shiftCounter) + ".tmx";
				Intent startGameIntent = new Intent(getActivity(), GameActivity.class);
				startGameIntent.putExtra(GAME_MAP_KEY, mapId);
				startActivity(startGameIntent);

				break;

			case R.id.slideLeftButton:
				shiftChooserButtons(false);
				break;

			case R.id.slideRightButton:
				shiftChooserButtons(true);
				break;
			}

		}

		return false;
	}

	private int shiftCounter = 0;
	private static final int ANIM_DURATION = 250;

	public void shiftChooserButtons(boolean shiftRight)
	{
		boolean shouldAnimate = true;
		boolean canAnimateRight = true;
		boolean canAnimateLeft = true;

		if (shiftRight)
		{
			shiftCounter++;
		}
		else
		{
			if (shiftCounter > 0)
			{
				shiftCounter--;
			}
			else
			{
				shouldAnimate = false;
				canAnimateLeft = false;
			}
		}

		slideLeftButton.setEnabled(true);
		slideRightButton.setEnabled(true);

		if (shouldAnimate)
		{

			if (shiftRight)
			{
				AnimatorSet set = new AnimatorSet();
				ObjectAnimator slideToLeftEdge = ObjectAnimator.ofFloat(rootView, "x", -rootView.getWidth());
				ObjectAnimator slideFromRightEdge = ObjectAnimator.ofFloat(rootView, "x", rootView.getWidth() + rootView.getRight(),
						rootView.getLeft());

				slideToLeftEdge.setDuration(ANIM_DURATION);
				slideToLeftEdge.addListener(onEndSlideAnimatorListener);

				slideFromRightEdge.setDuration(ANIM_DURATION);

				set.playSequentially(slideToLeftEdge, slideFromRightEdge);
				set.start();
			}
			else
			{
				AnimatorSet set = new AnimatorSet();
				ObjectAnimator slideFromLeftEdge = ObjectAnimator.ofFloat(rootView, "x", rootView.getWidth() + rootView.getRight());
				ObjectAnimator slideToRightEdge = ObjectAnimator.ofFloat(rootView, "x", -rootView.getWidth(), rootView.getLeft());

				slideFromLeftEdge.setDuration(ANIM_DURATION);
				slideFromLeftEdge.addListener(onEndSlideAnimatorListener);
				
				slideToRightEdge.setDuration(ANIM_DURATION);

				set.playSequentially(slideFromLeftEdge, slideToRightEdge);
				set.start();
			}

		}
		else
		{
			if (!canAnimateLeft)
			{
				// slideLeftButton.setEnabled(false);
			}
			else if (!canAnimateRight)
			{
				// slideRightButton.setEnabled(false);
			}
		}

	}

	private AnimatorListener onEndSlideAnimatorListener = new AnimatorListener()
	{

		@Override
		public void onAnimationStart(Animator arg0)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationRepeat(Animator arg0)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationEnd(Animator arg0)
		{
			for (int i = 0; i < levelChooserButtons.size(); i++)
			{
				final View rootLayoutButton = levelChooserButtons.get(i);
				final int index = i;
				Button chooseButton = (Button) rootLayoutButton.findViewById(R.id.chooser_button);
				chooseButton.setText(index + 1 + NUM_BUTTONS * shiftCounter + "");
			}
		}

		@Override
		public void onAnimationCancel(Animator arg0)
		{
		}
	};
}
