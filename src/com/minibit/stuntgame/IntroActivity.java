package com.minibit.stuntgame;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class IntroActivity extends FragmentActivity
{

	public static final String CARTOON_FONT = "Skranji/Skranji-Regular.ttf";
	private Fragment mainScreenFragment;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.main);

		FragmentManager manager = getSupportFragmentManager();
		mainScreenFragment = new IntroFragment();
		// Add fragment programatically because it can't be replaced if defined in xml
		manager.beginTransaction().add(R.id.rootLayout, mainScreenFragment).commit();

		Display display = getWindowManager().getDefaultDisplay();
		CloudAnimator animator = new CloudAnimator(5, new Point(display.getWidth(), display.getHeight()),
				(RelativeLayout) findViewById(R.id.rootLayout));
		animator.animateClouds();
	}

	public void singlePlayerButtonClicked(View view)
	{
		FragmentManager manager = getSupportFragmentManager();
		manager.beginTransaction().replace(R.id.rootLayout, new LevelChooserFragment()).addToBackStack(null).commit();
	}

}
