package com.minibit.stuntgame;

import android.app.Service;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.minibit.box2dLevelEditor.BasePhysicsGame.BroadLevelGameListener;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ObjectAnimator;

public class GameActivity extends AndroidApplication implements OnTouchListener,
		BroadLevelGameListener
{
	private View pauseMenu, uiOverlay, levelClearedMenu;
	private RelativeLayout rootLayout;
	private StuntGame game;

	public static final String DEBUG_TAG = "Minibit Debug";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		String mapId = "";

		if (getIntent().getExtras() != null)
		{
			mapId = getIntent().getStringExtra(LevelChooserFragment.GAME_MAP_KEY);
		}

		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGL20 = true;
		cfg.useAccelerometer = true;
		game = new StuntGame(mapId, this);

		rootLayout = new RelativeLayout(this);
		View gameView = initializeForView(game, cfg);
		rootLayout.addView(gameView);

		LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
		uiOverlay = inflater.inflate(R.layout.game_layout, rootLayout, false);
		pauseMenu = inflater.inflate(R.layout.pause_menu, rootLayout, false);
		levelClearedMenu = inflater.inflate(R.layout.level_cleared_menu, rootLayout, false);

		rootLayout.addView(uiOverlay);
		setContentView(rootLayout);

		Utils.addTouchListeners(uiOverlay, this);
		Utils.addTouchListeners(pauseMenu, this);
		Utils.addTouchListeners(levelClearedMenu, this);
	}

	private boolean isPaused = false;

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		Utils.handleTouch(v, event);
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			switch (v.getId())
			{
			case R.id.pauseButton:
				isPaused = !isPaused;
				if (isPaused)
				{
					rootLayout.addView(pauseMenu);
				}
				else
				{
					rootLayout.removeView(pauseMenu);
				}

				game.gameStateChanged(isPaused);
				break;

			case R.id.playMenuButton:
				rootLayout.removeView(pauseMenu);
				game.gameStateChanged(false);
				break;

			case R.id.restartMenuButton:
				rootLayout.removeView(pauseMenu);
				game.scheduleRestart();
				game.gameStateChanged(false);
				break;

			case R.id.settingsMenuButton:
				rootLayout.removeView(pauseMenu);
				game.debugButtonClicked();
				game.gameStateChanged(false);
				break;
			default:
				break;
			}
		}
		return false;
	}

	@Override
	public void clearedLevel(int timeToComplete)
	{
		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				// Remove the pause button; it doesn't serve any function once the level cleared
				// menu is up
				rootLayout.removeView(uiOverlay);
				rootLayout.addView(levelClearedMenu);

				ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(levelClearedMenu, "alpha", 0,
						1.0f);

				AnimatorSet set = new AnimatorSet();
				set.play(alphaAnimator);
				set.setInterpolator(new AccelerateInterpolator());
				set.setDuration(1500);
				set.start();

				set.addListener(new AnimatorListener()
				{

					@Override
					public void onAnimationStart(Animator arg0)
					{

					}

					@Override
					public void onAnimationRepeat(Animator arg0)
					{

					}

					@Override
					public void onAnimationEnd(Animator arg0)
					{
						game.setGamePaused(true);
					}

					@Override
					public void onAnimationCancel(Animator arg0)
					{}
				});
			}
		});
	}
}