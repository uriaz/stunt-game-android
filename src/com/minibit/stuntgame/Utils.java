package com.minibit.stuntgame;

import java.util.ArrayList;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public class Utils
{
	public static void applyButtonScaleAnim(View view, boolean zoomIn, float zoomAmount)
	{
		float smallZoom = 1 / zoomAmount;
		float zoomScale = zoomIn ? smallZoom : zoomAmount;

		ObjectAnimator scaleAnimator = ObjectAnimator.ofFloat(view, "scaleX", zoomScale);
		ObjectAnimator scaleAnimatorY = ObjectAnimator.ofFloat(view, "scaleY", zoomScale);
		scaleAnimator.setDuration(100);
		scaleAnimatorY.setDuration(100);

		AnimatorSet set = new AnimatorSet();
		set.playTogether(scaleAnimator, scaleAnimatorY);
		set.start();
	}

	/**
	 * Adds touch listeners to all elements within a view
	 * 
	 * @param rootView
	 *            Root view whose views touch listeners are added to
	 * @param listener
	 */
	public static void addTouchListeners(View rootView, OnTouchListener listener)
	{
		ArrayList<View> touchables = rootView.getTouchables();
		for (int i = 0; i < touchables.size(); i++)
		{
			touchables.get(i).setOnTouchListener(listener);
		}
	}

	/**
	 * Apply a scale animation to the touched view
	 * 
	 * @param v
	 * @param event
	 */
	public static void handleTouch(View v, MotionEvent event)
	{
		switch (event.getAction())
		{
		case MotionEvent.ACTION_DOWN:
			Utils.applyButtonScaleAnim(v, true, 1.15f);
			break;

		case MotionEvent.ACTION_UP:
			Utils.applyButtonScaleAnim(v, false, 1.0f);
			break;
		}
	}
}
