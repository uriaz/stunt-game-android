package com.minibit.stuntgame;

public class LevelDescriptor
{

	private int levelNum;
	private int activeStars;
	private int mapPack;

	public LevelDescriptor()
	{
	}

	public int getLevelNum()
	{
		return levelNum;
	}

	public void setLevelNum(int levelNum)
	{
		this.levelNum = levelNum;
	}

	public int getActiveStars()
	{
		return activeStars;
	}

	public void setActiveStars(int activeStars)
	{
		this.activeStars = activeStars;
	}

	public int getMapPack()
	{
		return mapPack;
	}

	public void setMapPack(int mapPack)
	{
		this.mapPack = mapPack;
	}

}
